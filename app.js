var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var fs = require("fs");
var cors = require("cors");
app.use(cors());

app.use(bodyParser.json());

StartBot();

app.post("/send", function(req, res) {

  var data = JSON.stringify(req.body)
  
  console.log(data)

  function getCurrentDayTimestamp() {
    const d = new Date()

    return new Date(
      Date.UTC(
        d.getFullYear(),
        d.getMonth(),
        d.getDate(),
        d.getHours(),
        d.getMinutes(),
        d.getSeconds()
      )
    ).toISOString().slice(0, 16)
  }

  let path = 'contacts/' + getCurrentDayTimestamp() + '-' + req.body.name + '.json'


  fs.writeFile(path, data, err => {
    
  });

  res.send("Ok");
});

app.listen(3002, function() {
  console.log("Example app listening on port 3002!");
});

async function StartBot() {
  var file = require("fs");
  const TelegramBot = require("node-telegram-bot-api");

  const token = "640153765:AAHPNnZm1assDaGZpbuW0emzP2R3Zu6Mv4g";
  const chatId = -280352283;

  const bot = new TelegramBot(token, { polling: true });

  let whiteList = [];

  await bot.getChatAdministrators(chatId)
    .then(resolve => {
      resolve.forEach(user => {
        whiteList.push(user.user.id);
      });
    })
    .catch(reject => {
      console.log(reject);
    });

  bot.onText(/\/list/, (msg, match) => {
    if (whiteList.find(i => i === msg.from.id) !== -1) {
      fs.readdir("contacts", function(err, items) {
        for (var i = 0; i < items.length; i++) {
          bot.sendMessage(chatId, items[i]);
        }
      });
    }
  });

  bot.onText(/\/show (.+)/, (msg, match) => {
    if (whiteList.find(i => i === msg.from.id) !== -1) {
      fs.readFile("contacts/" + match[1], (err, contents) => {
        contents = JSON.parse(contents)
        var text = "\nИмя: " + contents.name + "\nEmail: " + contents.email + "\nТелефон: " + contents.phone + "\n" + contents.text;
        bot.sendMessage(chatId, text);
      })
    }
  });

  
}